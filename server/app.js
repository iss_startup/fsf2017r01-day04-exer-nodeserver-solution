// TODO: 1. Load/import the module required by this application
// Load/import the http module. This module gives us access to the HTTP server and client
var http = require('http');

/* Define server port.
 Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
 */
const NODE_PORT = process.env.NODE_PORT || 3000;

// Create server object
var server = http.createServer(handleRequest);

// Define handleRequest function
// handleRequest handles requests and sends responses
function handleRequest(request, response){
    response.end('It Works!! Path Hit: ' + request.url);
}

// Start servers
server.listen(NODE_PORT, function(){
    // Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", NODE_PORT);
});